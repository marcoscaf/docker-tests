FROM ubuntu:latest
LABEL maintainer="Marcos Ferreira"
ENV FOLDER_HOME=/home/file
RUN apt-get update
RUN mkdir -p /home/file
COPY *.txt /home/file
WORKDIR /home/file
ENTRYPOINT ls -la && echo $FOLDER_HOME && /bin/bash
